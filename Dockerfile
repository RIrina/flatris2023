FROM node:15.14.0

RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN yarn install

COPY . /app

#RUN yarn test
RUN yarn build

#Oh hai
#yarn start

EXPOSE 3000

CMD yarn start


